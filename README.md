# Demisemiquaver

Program guide and music info.
Dev test for BMAT.


## Table of contents
1. [Running](https://gitlab.com/francopelusso/demisemiquaver#running)
1. [Loading data](https://gitlab.com/francopelusso/demisemiquaver#loading-data)
1. [Music Report CLI](https://gitlab.com/francopelusso/demisemiquaver#music-report-cli)
1. [Programs API](https://gitlab.com/francopelusso/demisemiquaver#programs-api)
1. [Programs List/Query View](https://gitlab.com/francopelusso/demisemiquaver#programs-listquery-view)


## Running

Either running with or without Docker, you need to have a local copy of the project on your machine.
For example, you can clone this gitlab repository by first installing [Git](https://git-scm.com/) and then cloning it: `git clone https://gitlab.com/francopelusso/demisemiquaver.git`.

Afterwards, you must configure your environment in a **.env** file. To save time, you may use the exact **.env.example** file provided by copying its content on **.env**. You can do this by running `cp .env.example .env` from within the project's folder.

Finally, you must apply migrations to create all database models: `python manage.py migrate`.

### Docker

The encouraged way of running this project is using Docker together with Docker-Compose. By doing this, you will have an instance running quickly without any extra effort nor configuration issues.

1. First of all, you must install Docker and Docker-Compose:
    * [Installing Docker on Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04) - [Installing Docker-Compose on Ubuntu](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04).
    * [Installing Docker on Debian](https://docs.docker.com/install/linux/docker-ce/debian/)
    * [Installing Docker on Fedora](https://docs.docker.com/install/linux/docker-ce/fedora/)
    * [Installing Docker on CentOS](https://docs.docker.com/install/linux/docker-ce/centos/)
    * [Installing Docker on ArchLinux](https://wiki.archlinux.org/index.php/Docker#Installation)
    * [Installing Docker-Compose on Linux](https://docs.docker.com/compose/install/)
    * [Installing Docker on Windows](https://docs.docker.com/docker-for-windows/install/) (Docker-Compose is included).
1. Afterwards, get into the project's root folder. For example: `cd ~/demisemiquaver`.
1. From within this folder, build docker's containers with the following command: `docker-compose build`.
1. Once ready, start docker's containers: `docker-compose up`.
1. Now you will be able to access Django's website at [http://localhost:8000/](http://localhost:8000/).
1. **That's all folks!**

### Running without Docker

Although you can configure this project to run in a production-like environment using WSGI or ASGI and Nginx or Apache, I highly recommend running only Django's development server outside of Docker, at least for testing purposes.

#### Running development server outside Docker

1. Install requirements: `pip install -r requirements.txt`.
1. Run server using Django's manage.py file: `python manage.py runserver`.
1. The website will be available at [http://localhost:8000/](http://localhost:8000/).
1. **That's all folks!**

### Running tests

If you want to run tests (`python manage.py test`), you should install both requirements.txt and requirements_dev.txt: `pip install -r requirements.txt -r requirements_dev.txt`.


## Loading data

To make programs' data available through the web API, it must be loaded into the database used by Django. To make this process easily repeatable, some useful [Django's commands](https://docs.djangoproject.com/en/3.0/howto/custom-management-commands/) have been added into **hum** app which can load data for both programs and aired songs from certain files' format. However, focus is on extensibility, so other formats should be easy to add too.

### Loading programs' data from a CSV file

You can import programs' data from a CSV file by running the following command:

```bash
python manage.py import_programs_csv FILENAME
```

For example: `python manage.py import_programs_csv ~/epg.csv`.

#### CSV format

```
channel_id,channel_name,channel_country,start_date,start_time,duration_in_seconds,program_id,program_original_title,program_local_title,program_year
123456789,Example Channel,ESP,20200101,00:00,3600,123456,Programa de Ejemplo,Example Program,2020
```

### Loading aired songs' data from a JSON file

You can import aired songs' data from a JSON file by running the following command:

```bash
python manage.py import_airings_json FILENAME
```

For example: `python manage.py import_airings_json ~/matches-hd1.csv`.

#### JSON format

The file could be a valid JSON array of objects with the following structure:

```json
[
    {
        "title": "Never Gonna Give You Up",
        "length": 12,
        "album": "Whenever You Need Somebody",
        "artist": "Rick Astley",
        "start_time_utc": "2020-02-01T06:34:01.000000"
    },
    {
        "title": "Don't Go Breaking My Heart",
        "length": 10,
        "album": "Rock of the Westies",
        "artist": "Elton John",
        "start_time_utc": "2020-02-03T18:34:01.000000"
    }
]
```

However, it can also be a file containing a JSON object per line as follows:

```json
{"title": "Never Gonna Give You Up", "length": 12, "album": "Whenever You Need Somebody", "artist": "Rick Astley", "start_time_utc": "2020-02-01T06:34:01.000000"}
{"title": "Don't Go Breaking My Heart", "length": 10, "album": "Rock of the Westies", "artist": "Elton John", "start_time_utc": "2020-02-03T18:34:01.000000"}
```


## Music Report CLI

As well as importing data, exporting it is also a key feature for the **hum** app. A music report containing all songs aired within a date-time range in a certain channel can be generated by running **export_airings_xlsx** Django's command.
As the name suggests, the output will be a **.xlsx** file containing a song in each row together with the program in which it was aired.

```bash
python manage.py export_airings_xlsx START_DATE START_TIME END_DATE END_TIME CHANNEL_NAME [-f --filename FILENAME]
```

For example:

```bash
# Output file's name will be airings_hd1.xlsx
python manage.py export_airings_xlsx 20200101 00:00 20200102 12:30 'HD1 (France)' -f airings_hd1
```

```bash
# Output file's name will be airings_rte_1_202001010000_202001021230.xlsx
python manage.py export_airings_xlsx 20200101 00:00 20200102 12:30 'RTE 1'
```


## Programs API

Programs' data can be retrieved from a REST API located at **/api/hum/programs/**. For example, when running at localhost, you can access it at [http://localhost:8000/api/hum/programs/](http://localhost:8000/api/hum/programs/).

### Parameters

* **start_date**: Formatted as YYYY-MM-DD.
* **end_date**: Formatted as YYYY-MM-DD.
* **title** (*Optional*): Exact program's local title to search for.
* **country** (*Optional*): Exact channel's country to search programs in.
* **size** (*Optional*): Number of results to show per page.
* **page** (*Optional*): Number of page to retrieve.

After adding this parameters, the following would be valid requests:

* http://localhost:8000/api/hum/programs/?start_date=2020-01-01&end_date=2020-01-03
* http://localhost:8000/api/hum/programs/?start_date=2020-01-01&end_date=2020-01-03&title=Example Program
* http://localhost:8000/api/hum/programs/?start_date=2020-01-01&end_date=2020-01-03&country=FRA
* http://localhost:8000/api/hum/programs/?start_date=2020-01-01&end_date=2020-01-03&size=5&page=3

**Note that start_date and end_date are mandatory.**


## Programs List/Query View

To make programs' data available for all non-techie users too, an HTTP/HTML interface is also included in **hum** app.
It is located at **/hum/programs/** and, for this test's scope, it has also been routed to every other non-matching path. You can access it at [http://localhost:8000/hum/programs/](http://localhost:8000/hum/programs/).

This view asks for a start and end date and, optionally, for title and country also and makes a request to the aforementioned API. Then loads data into a table with server-side pagination.
