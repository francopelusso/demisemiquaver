from rest_framework.pagination import PageNumberPagination


class SizeParamPageNumberPagination(PageNumberPagination):
    page_size_query_param = 'size'
