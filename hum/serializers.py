from rest_framework import serializers

from hum.models import Program


class ProgramSerializer(serializers.ModelSerializer):
    channel_id = serializers.IntegerField(source='channel.external_id')
    channel_name = serializers.CharField(source='channel.name')
    channel_country = serializers.CharField(source='channel.country')
    program_id = serializers.IntegerField(source='external_id')
    program_local_title = serializers.CharField(source='local_title')
    program_original_title = serializers.CharField(source='original_title')
    program_year = serializers.IntegerField(source='year')

    class Meta:
        model = Program
        fields = [
            'channel_id',
            'channel_name',
            'channel_country',
            'program_id',
            'program_local_title',
            'program_original_title',
            'start_time',
            'end_time',
            'program_year'
        ]
