# Generated by Django 3.0.2 on 2020-02-01 22:10

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('hum', '0002_auto_20200201_1923'),
    ]

    operations = [
        migrations.AddField(
            model_name='program',
            name='end_time',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]
