from django.urls import path

from hum import views


app_name = 'hum'

urlpatterns = [
    path('programs/', views.ProgramListQuery.as_view(), name='program-query'),
]

api_urlpatterns = [
    path('programs/', views.ProgramListAPI.as_view(), name='program-list'),
]
