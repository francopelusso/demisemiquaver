from django.apps import AppConfig


class HumConfig(AppConfig):
    name = 'hum'

    def ready(self):
        import hum.signals
