from django.db import models


class Channel(models.Model):
    external_id = models.PositiveIntegerField(unique=True)
    name = models.CharField(max_length=255)
    country = models.CharField(max_length=3)


class Program(models.Model):
    external_id = models.PositiveIntegerField()
    local_title = models.CharField(max_length=255)
    original_title = models.CharField(max_length=255, null=True, blank=True)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    duration = models.PositiveIntegerField(help_text='Seconds')
    year = models.PositiveIntegerField(
        null=True,
        blank=True,
        help_text='YYYY'
    )
    channel = models.ForeignKey(
        'Channel',
        on_delete=models.CASCADE,
        related_name='programs'
    )


class Song(models.Model):
    title = models.CharField(max_length=255)
    album = models.CharField(max_length=255, null=True, blank=True)
    artist = models.CharField(max_length=255)


class Airing(models.Model):
    start_time = models.DateTimeField()
    length = models.PositiveIntegerField(help_text='Seconds')
    channel = models.ForeignKey(
        'Channel',
        on_delete=models.CASCADE,
        related_name='airings'
    )
    song = models.ForeignKey(
        'Song',
        on_delete=models.CASCADE,
        related_name='airings'
    )

    def get_program(self):
        try:
            return Program.objects.filter(
                channel=self.channel,
                start_time__lte=self.start_time,
                end_time__gte=self.start_time
            ).order_by('-start_time')[0]
        except IndexError:
            return None
