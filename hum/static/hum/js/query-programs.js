'use strict';

const startDateInput = document.getElementById('start_date');
const endDateInput = document.getElementById('end_date');
const countryInput = document.getElementById('country');
const titleInput = document.getElementById('title');
// Pagination variables
const paginator = document.getElementById('paginator');
let pageNumber = 1;
const pageSize = 50;
let resultsCount = 0;
let pageCount = 0;
// Use these names when showing errors instead of the inner slugified version.
const fieldnames = {start_date: 'Start Date', end_date: 'End Date'};

setTextsToDateOnFocus();
setRequestTimeout();

$('#table').bootstrapTable({
  columns: [
    {field: 'program_id', title: 'ID'},
    {field: 'program_local_title', title: 'Title'},
    {field: 'start_time', title: 'Start Time'},
    {field: 'end_time', title: 'End Time'},
    {field: 'channel_country', title: 'Country'},
    {field: 'program_original_title', title: 'Program Original Title'},
    {field: 'program_year', title: 'Year'},
    {field: 'channel_id', title: 'Channel ID'},
    {field: 'channel_name', title: 'Channel Name'}
  ],
  search: true,
  theadClasses: 'thead-dark',
});


function setTextsToDateOnFocus() {
  /* Use text inputs to show placeholder and date for widget when editing. */
  for (const el of [startDateInput, endDateInput]) {
    el.addEventListener('focus', () => { el.type = 'date';})
    el.addEventListener('blur', () => { el.type = 'text';})
  }
}

function setRequestTimeout() {
  /* Request programs list from API when inputs are changed.
     Leave 600ms to avoid requesting for every character written.
  */
  let timeout;
  for (const el of [startDateInput, endDateInput, countryInput, titleInput]) {
    el.addEventListener('input', () => {
      clearTimeout(timeout);
      timeout = setTimeout(makeRequest, 600);
    });
  }

  for (const el of [startDateInput, endDateInput]) {
    el.addEventListener('focus', () => {
      clearTimeout(timeout);
    });
    el.addEventListener('blur', () => {
      clearTimeout(timeout);
      timeout = setTimeout(makeRequest, 600);
    });
  }
}


function makeRequest() {
  /* Request programs from API with axios and reload table data if success.
     If there is some error, display it in the #errors div.
  */
  if (!startDateInput.value || !endDateInput.value) {
    showErrors('Must provide start and end dates.');
    return;
  }
  axios.get(APIUrl, {
    params: {
      start_date: startDateInput.value,
      end_date: endDateInput.value,
      country: countryInput.value,
      title: titleInput.value,
      size: pageSize,
      page: pageNumber
    }
  })
    .then(data => {
      $('#table').bootstrapTable('load', data.data.results);
      buildPagination(data.data);
      $('#errors').hide();
    })
    .catch(error => {
      let errorsHTML = '<ul>';
      for (const key in error.response.data) {
        errorsHTML += `<li>${fieldnames[key]}: ${error.response.data[key]}</li>`;
      }
      errorsHTML += '</ul>';
      showErrors(errorsHTML);
    });
}

function buildPagination(data) {
  /* Show or update paginator. */
  resultsCount = data.count;
  pageCount = Math.ceil(resultsCount/pageSize);
  const paginatorHTML = `
    <li class="page-item ${data.previous ? '' : 'disabled'}">
      <a class="page-link" href="#" onclick="changePage('first');">First</a>
    </li>
    <li class="page-item ${data.previous ? '' : 'disabled'}">
      <a class="page-link" href="#" onclick="changePage('previous');"><</a>
    </li>
    ${data.previous ? '<li class="page-item"><a class="page-link">...</a></li>': ''}
    <li class="page-item active">
      <a class="page-link">${pageNumber}</a>
    </li>
    ${data.next ? '<li class="page-item"><a class="page-link">...</a></li>': ''}
    <li class="page-item ${data.next ? '' : 'disabled'}">
      <a class="page-link" href="#" onclick="changePage('next');">></a>
    </li>
    <li class="page-item ${data.next ? '' : 'disabled'}">
      <a class="page-link" href="#" onclick="changePage('last');">Last</a>
    </li>
    <p id="page-count" class="m-0 pl-2">Page ${pageNumber} of ${pageCount}</p>
  `;
  paginator.innerHTML = paginatorHTML;
}

function changePage(page) {
  switch(page) {
    case 'first':
      pageNumber = 1;
      break;
    case 'previous':
      pageNumber -= 1;
      break;
    case 'next':
      pageNumber += 1;
      break;
    case 'last':
      pageNumber = Math.ceil(pageCount);
  }
  makeRequest();
}


function showErrors(errorsHTML) {
  $('#errors').html(`<h3>Errors</h3>${errorsHTML}`);
  $('#errors').show();
}
