import os
import xlrd

from django.core.management import call_command
from django.test import TestCase
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from hum.models import Airing, Channel, Program, Song


TESTS_PATH = os.path.dirname(os.path.realpath(__file__))


def load_programs_data():
    call_command(
        'import_programs_csv',
        os.path.join(TESTS_PATH, 'test_files', 'test_epg.csv')
    )


def load_airings_data():
    call_command(
        'import_airings_json',
        os.path.join(
            TESTS_PATH,
            'test_files',
            'test_matches-test_channel_1.json'
        )
    )


def export_airings_data():
    call_command(
        'export_airings_xlsx',
        '20200101', '00:00',
        '20200101', '23:59',
        'Test Channel 1',
        '-f', 'exported_airings'
    )


class ProgramAPITests(APITestCase):
    def test_without_time(self):
        url = reverse('hum-api:program-list')
        response = self.client.get(url)
        data = response.json()
        expected_data = {
            'start_date': 'Missing parameter.',
            'end_date': 'Missing parameter.'
        }
        self.assertEqual(response.status_code, 400)
        self.assertEqual(data, expected_data)

    def test_invalid_time(self):
        url = reverse('hum-api:program-list')
        params = {'start_date': '2020-01-01', 'end_date': 'ABC'}
        response = self.client.get(url, params)
        data = response.json()
        expected_data = {"end_date": "Bad format. Must match %Y-%m-%d."}
        self.assertEqual(response.status_code, 400)
        self.assertEqual(data, expected_data)

    def test_query_by_time(self):
        load_programs_data()
        url = reverse('hum-api:program-list')
        params = {'start_date': '2020-01-01', 'end_date': '2020-01-01'}
        response = self.client.get(url, params)
        data = response.json()
        expected_data = {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [
                {
                    "channel_id": 1234567,
                    "channel_name": "Test Channel 1",
                    "channel_country": "TC1",
                    "program_id": 1234,
                    "program_local_title": "Test Program 1",
                    "program_original_title": "Test Program 1",
                    "start_time": "2020-01-01T00:00:00Z",
                    "end_time": "2020-01-01T23:59:59Z",
                    "program_year": 2010
                }
            ]
        }
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, expected_data)

    def test_query_by_title(self):
        load_programs_data()
        url = reverse('hum-api:program-list')
        params = {
            'start_date': '2020-01-01',
            'end_date': '2020-01-03',
            'title': 'Test Program 2'
        }
        response = self.client.get(url, params)
        data = response.json()
        expected_data = {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [
                {
                    "channel_id": 123456789,
                    "channel_name": "Test Channel 2",
                    "channel_country": "TC2",
                    "program_id": 12345,
                    "program_local_title": "Test Program 2",
                    "program_original_title": "Test Program 2",
                    "start_time": "2020-01-02T00:00:00Z",
                    "end_time": "2020-01-02T23:59:59Z",
                    "program_year": 2020
                }
            ]
        }
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, expected_data)

    def test_query_by_country(self):
        load_programs_data()
        url = reverse('hum-api:program-list')
        params = {
            'start_date': '2020-01-01',
            'end_date': '2020-01-03',
            'country': 'TC1'
        }
        response = self.client.get(url, params)
        data = response.json()
        expected_data = {
            "count": 1,
            "next": None,
            "previous": None,
            "results": [
                {
                    "channel_id": 1234567,
                    "channel_name": "Test Channel 1",
                    "channel_country": "TC1",
                    "program_id": 1234,
                    "program_local_title": "Test Program 1",
                    "program_original_title": "Test Program 1",
                    "start_time": "2020-01-01T00:00:00Z",
                    "end_time": "2020-01-01T23:59:59Z",
                    "program_year": 2010
                }
            ]
        }
        self.assertEqual(response.status_code, 200)
        self.assertEqual(data, expected_data)


class ImportProgramsCSVTest(TestCase):
    def test_import(self):
        load_programs_data()
        self.assertGreater(Channel.objects.count(), 0)
        self.assertGreater(Program.objects.count(), 0)

    def test_remove_duplicates(self):
        load_programs_data()
        self.assertEqual(Channel.objects.count(), 2)
        self.assertEqual(Program.objects.count(), 2)


class ImportAiringsJSONTest(TestCase):
    def test_import(self):
        load_programs_data()
        load_airings_data()
        self.assertGreater(Song.objects.count(), 0)
        self.assertGreater(Airing.objects.count(), 0)

    def test_remove_duplicates(self):
        load_programs_data()
        load_airings_data()
        self.assertEqual(Song.objects.count(), 2)
        self.assertEqual(Airing.objects.count(), 2)


class ExportAiringsXLSXTest(TestCase):
    def test_output(self):
        load_programs_data()
        load_airings_data()
        export_airings_data()
        workbook = xlrd.open_workbook('exported_airings.xlsx')
        sheet = workbook.sheet_by_index(0)
        self.assertEqual(sheet.row(1)[1].value, 'Test Program 1')
        self.assertEqual(sheet.row(1)[2].value, 'Never Gonna Give You Up')
        os.remove('exported_airings.xlsx')
