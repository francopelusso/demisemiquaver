
import logging
from django.core.management.base import BaseCommand, CommandError

from hum.import_export.airings import JSONAiringImporter

logger = logging.getLogger('commands')


class Command(BaseCommand):
    help = 'Loads airings data from a JSON into DB'

    def add_arguments(self, parser):
        parser.add_argument('filename')

    def handle(self, *args, **options):
        logger.info(f"Starting import from {options['filename']}")
        JSONAiringImporter(options['filename']).load()
        logger.info('Finished importing airings.')
