
import logging
from django.core.management.base import BaseCommand, CommandError

from hum.import_export.programs import CSVProgramImporter

logger = logging.getLogger('commands')


class Command(BaseCommand):
    help = 'Loads program data from a CSV into DB'

    def add_arguments(self, parser):
        parser.add_argument('filename')

    def handle(self, *args, **options):
        logger.info(f"Starting import from {options['filename']}")
        CSVProgramImporter(options['filename']).load()
        logger.info('Finished importing programs.')
