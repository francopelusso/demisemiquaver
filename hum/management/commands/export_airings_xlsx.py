
import logging
import os
import pytz
import xlsxwriter
from datetime import datetime

from django.core.management.base import BaseCommand, CommandError
from django.db.models import Q
from django.utils.text import slugify

from hum.import_export.airings import XLSXAiringExporter
from hum.models import Airing, Channel


logger = logging.getLogger('commands')
DATE_FORMAT = '%Y%m%d'
TIME_FORMAT = '%H:%M'
DATETIME_FORMAT = f'{DATE_FORMAT}{TIME_FORMAT}'
TIMEZONE = pytz.timezone(os.getenv('AIRING_TIMEZONE', 'UTC'))


def mkdate(date_string):
    return datetime.strptime(date_string, DATE_FORMAT).date()


def mktime(time_string):
    return datetime.strptime(time_string, TIME_FORMAT).time()


def get_channel(channel_search):
    try:
        channel = Channel.objects.get(name__iexact=channel_search)
    except Channel.DoesNotExist:
        try:
            channel = Channel.objects.get(
                Q(pk=channel_search)
                | Q(external_id=channel_search)
            )
        except Channel.DoesNotExist:
            raise ValueError
    return channel


class Command(BaseCommand):
    help = 'Export airings from DB to a .xlsx file'

    def add_arguments(self, parser):
        parser.add_argument('start_date', type=mkdate)
        parser.add_argument('start_time', type=mktime)
        parser.add_argument('end_date', type=mkdate)
        parser.add_argument('end_time', type=mktime)
        parser.add_argument('channel', type=get_channel)
        parser.add_argument('-f', '--filename', required=False)

    def handle(self, *args, **options):
        self._wrangle_options(options)
        logger.info(
            f"Start exporting airings on {options['channel'].name} "
            f"from {options['start_datetime']} to {options['end_datetime']}"
        )
        XLSXAiringExporter(
            options['filename'],
            self._get_query_params(options)
        ).export()
        logger.info(f"Finished exporting airings to {options['filename']}")

    def _get_query_params(self, options):
        return {
            'start_time__gte': options['start_datetime'],
            'start_time__lte': options['end_datetime'],
            'channel': options['channel']
        }

    def _wrangle_options(self, options):
        options['start_datetime'] = TIMEZONE.localize(datetime.combine(
            options['start_date'],
            options['start_time']
        ))
        options['end_datetime'] = TIMEZONE.localize(datetime.combine(
            options['end_date'],
            options['end_time']
        ))
        if options['filename'] is None:
            options['filename'] = self._get_default_filename(options)
        if not options['filename'].endswith('.xlsx'):
            options['filename'] += '.xlsx'

    def _get_default_filename(self, options):
        dt_format = DATETIME_FORMAT.replace(':', '')
        return (
            'airings_'
            f"{slugify(options['channel'].name, allow_unicode=False)}_"
            f"{options['start_datetime'].strftime(dt_format)}_"
            f"{options['end_datetime'].strftime(dt_format)}"
        )
