import csv
import logging
import os
import pytz
from abc import ABC, abstractmethod
from datetime import datetime, timedelta

from django.conf import settings

from hum.import_export.base import BaseImporter
from hum.models import Channel, Program


logger = logging.getLogger('commands')


class ProgramImporter(BaseImporter, ABC):
    TIMEZONE = pytz.timezone(
        os.getenv('PROGRAM_TIMEZONE', settings.TIME_ZONE)
    )
    FIELDNAMES = {
        'external_id': ['program_id'],
        'local_title': ['program_local_title'],
        'original_title': ['program_original_title'],
        'duration': ['duration_in_seconds'],
        'year': ['program_year'],
        'channel_id': ['channel_id'],
        'channel_name': ['channel_name'],
        'channel_country': ['channel_country'],
        'start_date': ['start_date'],
        'start_time': ['start_time']
    }
    EXCLUDED_COLUMNS = [
        'channel_name',
        'channel_id',
        'channel_country',
        'start_date',
        'start_time'
    ]
    DATETIME_FORMAT = '%Y%m%d%H:%M'
    MODELS = [Program]
    UNIQUE_TOGETHER_FIELDS = [['external_id', 'channel_id', 'start_time']]

    @abstractmethod
    def _get_internal_names_from_columns(self, reader):
        raise NotImplementedError

    def _before_bulk_hook(self, model, i):
        pass


class CSVProgramImporter(ProgramImporter):
    def _get_rows_iterator(self, programs_file):
        return csv.DictReader(programs_file)

    def _get_internal_names_from_columns(self, reader):
        fieldnames = {}
        for column in reader.fieldnames:
            for k, v in self.FIELDNAMES.items():
                if column in v:
                    fieldnames[k] = column
                    break
        return fieldnames

    def _wrangle_rows(self):
        with open(self.filename, 'r') as programs_file:
            reader = self._get_rows_iterator(programs_file)
            self.fieldnames = self._get_internal_names_from_columns(reader)
            for i, row in enumerate(reader):
                self.wrangled_rows[0].append(
                    Program(**self._wrangle_row(row))
                )
                logger.debug(f'{i+1} rows wrangled.')

    def _wrangle_row(self, row):
        parsed_row = {
            k: row[self.fieldnames[k]] or None for k in self.fieldnames
            if k not in self.EXCLUDED_COLUMNS
        }
        parsed_row['start_time'] = self._get_datetime_from_column(
            row['start_date'], row['start_time']
        )
        parsed_row['end_time'] = parsed_row['start_time'] + timedelta(
            seconds=int(parsed_row['duration'])
        )
        parsed_row['channel_id'] = self._get_or_create_channel_from_column(
            row['channel_name'], row['channel_id'], row['channel_country']
        )
        return parsed_row

    def _get_datetime_from_column(self, p_date, p_time):
        return self.TIMEZONE.localize(
            datetime.strptime(f'{p_date}{p_time}', self.DATETIME_FORMAT)
        )

    def _get_or_create_channel_from_column(self, c_name, c_id, c_country):
        channel, _ = Channel.objects.get_or_create(
            name=c_name,
            external_id=c_id,
            country=c_country
        )
        return channel.pk
