import os
from abc import ABC, abstractmethod

from django.db import connection


class BaseImporter(ABC):
    def __init__(self, filename):
        if not os.path.exists(filename):
            raise FileNotFoundError(f"{filename} doesn't exist.")
        self.filename = filename
        self.wrangled_rows = [[]]
        self.channels = set()

    def load(self):
        self._wrangle_rows()
        for i, Model in enumerate(self.MODELS):
            self._before_bulk_hook(Model, i)
            Model.objects.bulk_create(self.wrangled_rows[i])
            self.delete_duplicates(
                Model._meta.db_table,
                self.UNIQUE_TOGETHER_FIELDS[i]
            )

    def delete_duplicates(self, table_name, unique_together_fields):
        with connection.cursor() as cursor:
            cursor.execute(f"""
                DELETE FROM {table_name} WHERE id NOT IN (
                    SELECT MIN(id) FROM {table_name} GROUP BY
                    {', '.join(unique_together_fields)}
                )
            """)

    @abstractmethod
    def _get_rows_iterator(self, programs_file):
        raise NotImplementedError

    @abstractmethod
    def _wrangle_rows(self):
        raise NotImplementedError

    @abstractmethod
    def _before_bulk_hook(self, model, i):
        pass


class BaseExporter(ABC):
    def __init__(self, filename, query_filters={}):
        self.filename = filename
        self.rows = []
        self.query_filters = query_filters

    def export(self):
        for element in self.MODEL.objects.filter(
            **self.query_filters
        ).select_related(*self.RELATED_TO_SELECT):
            self.rows.append(self._prepare_row(element))
        self._write_output()

    @abstractmethod
    def _prepare_row(self, element):
        raise NotImplementedError

    @abstractmethod
    def _write_output(self):
        raise NotImplementedError
