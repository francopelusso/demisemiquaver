import json
import logging
import os
import pytz
import xlsxwriter
from abc import ABC
from datetime import datetime

from django.conf import settings

from hum.import_export.base import BaseImporter, BaseExporter
from hum.models import Airing, Channel, Song


logger = logging.getLogger('commands')


class AiringImporter(BaseImporter, ABC):
    TIMEZONE = pytz.timezone(os.getenv('AIRING_TIMEZONE', settings.TIME_ZONE))
    MODELS = [Song, Airing]
    UNIQUE_TOGETHER_FIELDS = [
        ['title', 'artist'],
        ['song_id', 'channel_id', 'start_time']
    ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.channel_id = self._get_channel_id_from_filename()
        self.read_songs = set()
        self.songs = []
        self.airings = []


class JSONAiringImporter(AiringImporter):
    DATETIME_FORMAT = '%Y-%m-%dT%H:%M:%S.%f'

    def _get_channel_id_from_filename(self):
        file_name = channel_name = os.path.split(self.filename)[1]
        channel_name = file_name.split('-')[1].split('.')[0].replace('_', ' ')
        channels = Channel.objects.filter(name__istartswith=channel_name)
        if not channels:
            raise ValueError(f"Couldn't find channel {channel_name}.")
        return channels[0].pk

    def _get_rows_iterator(self, songs_file):
        try:
            return json.load(songs_file)
        except json.JSONDecodeError:
            songs_file.seek(0)
            return (json.loads(l) for l in songs_file.readlines())

    def _wrangle_rows(self):
        with open(self.filename, 'r') as songs_file:
            rows = self._get_rows_iterator(songs_file)
            for i, row in enumerate(rows):
                if row['title'] not in self.read_songs:
                    self.read_songs.add(row['title'])
                    self.songs.append(Song(
                        title=row['title'],
                        artist=row['artist'],
                        album=row['album'])
                    )
                self.airings.append({
                    'start_time': self.TIMEZONE.localize(datetime.strptime(
                        row['start_time_utc'], self.DATETIME_FORMAT
                    )),
                    'length': row['length'],
                    'title': row['title']
                })
                logger.debug(f'{i+1} rows wrangled.')
        self.wrangled_rows[0] = self.songs
        self.wrangled_rows.append([])

    def _before_bulk_hook(self, Model, i):
        if Model is Airing:
            for airing in self.airings:
                title = airing.pop('title')
                airing['song_id'] = Song.objects.get(title=title).pk
                airing['channel_id'] = self.channel_id
                self.wrangled_rows[i].append(Airing(**airing))


class AiringExporter(BaseExporter, ABC):
    MODEL = Airing
    RELATED_TO_SELECT = ['song']


class XLSXAiringExporter(AiringExporter):
    DT_FORMAT = 'YYYYmmdd HH:MM:SS'
    DT_LENGTH = len(DT_FORMAT)

    def _get_headers(self):
        return {
            'program_start': 'Program Start',
            'program_title': 'Program Title',
            'song_title': 'Song Title',
            'song_artist': 'Song Artist',
            'song_start': 'Song Start',
            'song_duration': 'Song Duration'
        }

    def _get_columns_order(self):
        return {
            'program_start': 0,
            'program_title': 1,
            'song_title': 2,
            'song_artist': 3,
            'song_start': 4,
            'song_duration': 5
        }

    def _prepare_row(self, airing):
        program = airing.get_program()
        return {
            'program_start': program.start_time,
            'program_title': program.local_title,
            'song_title': airing.song.title,
            'song_artist': airing.song.artist,
            'song_start': airing.start_time,
            'song_duration': airing.length,
        }

    def _write_output(self):
        with xlsxwriter.Workbook(
            self.filename,
            {'default_date_format': self.DT_FORMAT, 'remove_timezone': True}
        ) as workbook:
            worksheet = workbook.add_worksheet()
            order = self._get_columns_order()
            lengths = {k: 0 for k in order.keys()}
            for k, v in self._get_headers().items():
                worksheet.write(0, order[k], v)
                lengths[k] = self._get_length(v)
            row_index = 1
            for row in self.rows:
                for k, v in row.items():
                    worksheet.write(row_index, order[k], v)
                    length = self._get_length(v)
                    if length > lengths[k]:
                        lengths[k] = length
                row_index += 1
            for column, length in lengths.items():
                worksheet.set_column(order[column], order[column], length)

    def _get_length(self, value):
        try:
            length = len(value)
        except TypeError:
            if isinstance(value, datetime):
                length = self.DT_LENGTH
            else:
                length = len(str(value))
        return length
