from datetime import datetime

from django.urls import reverse
from django.views.generic import TemplateView
from rest_framework import serializers
from rest_framework.generics import ListAPIView

from hum.models import Program
from hum.pagination import SizeParamPageNumberPagination
from hum.serializers import ProgramSerializer


class ProgramListAPI(ListAPIView):
    DATE_FORMAT = '%Y-%m-%d'
    queryset = Program.objects.all()
    serializer_class = ProgramSerializer
    pagination_class = SizeParamPageNumberPagination
    paginate_by = 10
    default_order_field = 'start_time'

    def filter_queryset(self, queryset):
        queryset = self._filter_by_date(queryset)
        country = self.request.query_params.get('country')
        if country:
            queryset = queryset.filter(channel__country__iexact=country)
        title = self.request.query_params.get('title')
        if title:
            queryset = queryset.filter(local_title__iexact=title)
        return queryset.order_by(self._get_order_clause())

    def _filter_by_date(self, queryset):
        start_date = self.request.query_params.get('start_date')
        end_date = self.request.query_params.get('end_date')
        start_date = self._parse_date(start_date, 'start_date')
        end_date = self._parse_date(end_date, 'end_date')
        if getattr(self, 'errors', None) is not None:
            raise serializers.ValidationError(self.errors)
        queryset = queryset.filter(
            start_time__date__gte=start_date, end_time__date__lte=end_date
        )
        return queryset

    def _parse_date(self, date_param, error_name):
        if date_param is None:
            self._append_error(error_name, 'Missing parameter.')
        else:
            try:
                date_param = datetime.strptime(
                    date_param, self.DATE_FORMAT
                ).date()
            except ValueError:
                self._append_error(
                    error_name,
                    f'Bad format. Must match {self.DATE_FORMAT}.'
                )
        return date_param

    def _append_error(self, error_name, error_value):
        if isinstance(getattr(self, 'errors', None), dict):
            self.errors[error_name] = error_value
        else:
            self.errors = {error_name: error_value}

    def _get_order_clause(self):
        sort = self.request.query_params.get('sort', self.default_order_field)
        descending = self.request.query_params.get('direction', '') == 'desc'
        return f"{'-' if descending else ''}{sort}"


class ProgramListQuery(TemplateView):
    template_name = 'hum/query-programs.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['programs_api_url'] = self.request.build_absolute_uri(
            reverse('hum-api:program-list')
        )
        return context
